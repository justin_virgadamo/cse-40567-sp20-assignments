#!/usr/bin/env python3

import sys
import os
import base64
import binascii
import secrets
import random

from Crypto.Cipher import AES

def genBytes(leng):
	return secrets.token_bytes(leng)

def encrypt(message):
	start   = random.randint(5, 10)
	end     = random.randint(5, 10)
	ptext   = (genBytes(start) + message + genBytes(end))
	pad     = (16 - len(ptext)) % 16
	message = ptext + genBytes(pad)

	key = bytes(genBytes(16))
	if random.randint(0, 1):
		c	= AES.new(key, AES.MODE_ECB)
		enc = c.encrypt(message)
		return [enc, 1]
	else:
		iv  = genBytes(16)
		c   = AES.new(key, AES.MODE_CBC, iv)
		enc = c.encrypt(message)
		return [enc, 0]

def detect(cip, method):
	dic  = {}
	nb   = round(len(cip) / 16)

	for i in range(0, nb):
		temp = cip[i*16: (i*16)+16]
		if temp in dic.keys():
			if method == 1:
				print("ECB Mode [Correct Detection]")
				return
			else:
				print("ECB Mode [Incorrect Detection]")
				return
		else:
			dic[temp] = 0;
	if method == 0:
		print("CBC Mode [Correct Detection]")
		return

for _ in range(30):
	tempText = encrypt(bytearray(b"jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"))
	detect(tempText[0], tempText[1])
