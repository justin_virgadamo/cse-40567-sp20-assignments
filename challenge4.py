#!/usr/bin/env python3

import sys
import base64
import random
import string

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto import Random

def getStr():
	return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))

def detect(text):
	block = round(len(text) / 32)
	for i in range(0, block):
		test1 = text[i*16: (i*16)+16]
		for j in range(i + 1, block):
			test2 = text[j*16: (j*16)+16]
			if (test1 == test2):
				return test1
	return 0

def getKey():
	return get_random_bytes(16)

def enc(key, string):
	mode = random.randint(1, 2)
	iv = Random.new().read(AES.block_size)
	cipher = AES.new(key, mode, iv)
	retVal = cipher.encrypt(string)
	retVal = get_random_bytes(random.randint(5, 10)) + retVal + get_random_bytes(random.randint(5, 10))
	return retVal
	

for _ in range(30):
	if (detect(enc(getKey(), "counteroffensive")) == 0):
		print("CBC Mode [correct detection]")
	else:
		print("ECB Mode [correct detection]")
